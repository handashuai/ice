package main

import (
	"fmt"

	"gitee.com/handashuai/ice/httpquery"
)

// queryoption queryoption
type queryoption struct {
	Query   string `url:"q"`
	ShowAll bool   `url:"showall"`
	Page    int    `url:"page"`
}

// ExampleQuery ExampleQuery
func ExampleQuery() {
	opt := queryoption{"foo", true, 2}
	v, _ := httpquery.Values(opt)
	fmt.Print(v.Encode()) // will output: "q=foo&all=true&page=2"
}
